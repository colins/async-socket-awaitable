﻿using System.Net.Sockets;

namespace AsyncCore
{
    public class SocketAwaitableEventArgs : SocketAsyncEventArgs
    {
        private readonly SocketAwaiter _awaiter;

        public SocketAwaitableEventArgs()
        {
            _awaiter = new SocketAwaiter(this);
        }

        public SocketAwaiter GetAwaiter()
        {
            return _awaiter;
        }
    }
}
