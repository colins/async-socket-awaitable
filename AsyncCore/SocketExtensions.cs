﻿using System;
using System.Net.Sockets;

namespace AsyncCore
{
    public static class SocketExtensions
    {
        public static SocketAwaiter AcceptAwaitableAsync(this Socket socket, SocketAwaitableEventArgs eventArgs)
        {
            return OperationAsync(socket, eventArgs, (s, args) => s.AcceptAsync(args));
        }

        public static SocketAwaiter ConnectAwaitableAsync(this Socket socket, SocketAwaitableEventArgs eventArgs)
        {
            return OperationAsync(socket, eventArgs, (s, args) => s.ConnectAsync(args));
        }

        public static SocketAwaiter DisconnectAwaitableAsync(this Socket socket, SocketAwaitableEventArgs eventArgs)
        {
            return OperationAsync(socket, eventArgs, (s, args) => s.DisconnectAsync(args));
        }

        public static SocketAwaiter ReceiveAwaitableAsync(this Socket socket, SocketAwaitableEventArgs eventArgs)
        {
            return OperationAsync(socket, eventArgs, (s, args) => s.ReceiveAsync(args));
        }

        public static SocketAwaiter SendAwaitableAsync(this Socket socket, SocketAwaitableEventArgs eventArgs)
        {
            return OperationAsync(socket, eventArgs, (s, args) => s.SendAsync(args));
        }

        private static SocketAwaiter OperationAsync(this Socket socket, SocketAwaitableEventArgs eventArgs, 
            Func<Socket, SocketAwaitableEventArgs, bool> operationAsync)
        {
            var awaiter = eventArgs.GetAwaiter();

            awaiter.Reset();

            var isPending = operationAsync(socket, eventArgs);

            if (!isPending)
            {
                awaiter.Complete();
            }

            return awaiter;
        }

    }
}
