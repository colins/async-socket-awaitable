﻿using System;

namespace AsyncCore
{
    public class TcpAsyncReceiveResult
    {
        public readonly int BytesTransferred;

        public byte[] Buffer => Segment.Array;

        public readonly ArraySegment<byte> Segment;

        public TcpAsyncReceiveResult(ArraySegment<byte> segment, int bytesTransferred)
        {
            Segment = segment;
            BytesTransferred = bytesTransferred;
        }
    }
}
