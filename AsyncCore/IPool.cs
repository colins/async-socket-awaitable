﻿namespace AsyncCore
{
    public interface IPool<T>
    {
        T Take();

        void Return(T value);
    }
}
