﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace AsyncCore
{
    public class TcpAsyncClient : IDisposable
    {
        private readonly static ArraySegment<byte> EmptySegment = new ArraySegment<byte>(new byte[0], 0, 0); 

        private Socket _socket;
        private readonly TaskQueue _sendQueue;
        private readonly IPool<ArraySegment<byte>> _segmentPool;
        private readonly IPool<SocketAwaitableEventArgs> _eventArgsPool;
        private readonly IProtocol<TcpAsyncClient> _protocol;

        public TcpAsyncClient(IPool<ArraySegment<byte>> segmentPool,
            IPool<SocketAwaitableEventArgs> eventArgsPool, IProtocol<TcpAsyncClient> protocol)
        {
            _sendQueue = new TaskQueue();
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            _protocol = protocol;
            _segmentPool = segmentPool;
            _eventArgsPool = eventArgsPool;
        }

        internal TcpAsyncClient(Socket socket, IPool<ArraySegment<byte>> segmentPool,
            IPool<SocketAwaitableEventArgs> eventArgsPool, IProtocol<TcpAsyncClient> protocol)
        {
            _sendQueue = new TaskQueue();
            _socket = socket;
            _protocol = protocol;
            _segmentPool = segmentPool;
            _eventArgsPool = eventArgsPool;
        }

        public Socket Socket => _socket;

        public void StartReceiving()
        {
            Task.Factory.StartNew(async () =>
            {
                try
                {
                    TcpAsyncReceiveResult result;

                    while ((result = await ReceiveAsync()) != null)
                    {
                        try
                        {
                            await _protocol.OnReceive(this, result);
                        }
                        catch (Exception e)
                        {
                            await _protocol.OnError(this, e);
                        }
                        finally
                        {
                            // TODO: wymyśl coś, żeby to było przezroczyste

                            _segmentPool.Return(result.Segment);
                        }
                    }

                    await _protocol.OnDisconnect(this);
                }
                catch (SocketException e)
                {
                    if (e.SocketErrorCode == SocketError.ConnectionReset)
                    {
                        await _protocol.OnDisconnect(this);

                        return;
                    }

                    await _protocol.OnError(this, e);
                }
                catch (ObjectDisposedException)
                {
                    // socket has been closed
                }
                catch (Exception e)
                {
                    await _protocol.OnError(this, e);
                }
            }, TaskCreationOptions.LongRunning);
        }

        public async Task ConnectAsync(IPEndPoint remoteEndPoint)
        {
            SocketAwaitableEventArgs eventArgs = null;

            try
            {
                eventArgs = _eventArgsPool.Take();

                eventArgs.UserToken = this;

                eventArgs.RemoteEndPoint = remoteEndPoint;

                await _socket.ConnectAwaitableAsync(eventArgs);

                await _protocol.OnConnect(this);
            }
            finally
            {
                if (eventArgs != null)
                {
                    _eventArgsPool.Return(eventArgs);
                }
            }
        }

        public async Task<TcpAsyncReceiveResult> ReceiveAsync()
        {
            SocketAwaitableEventArgs eventArgs = null;
            var segment = EmptySegment;

            try
            {
                eventArgs = _eventArgsPool.Take();

                segment = _segmentPool.Take();

                eventArgs.UserToken = this;

                eventArgs.SetBuffer(segment.Array, segment.Offset, segment.Count);

                await _socket.ReceiveAwaitableAsync(eventArgs);

                if (eventArgs.BytesTransferred <= 0) return null;

                return new TcpAsyncReceiveResult(segment, eventArgs.BytesTransferred);
            }
            catch (Exception)
            {
                if (segment != EmptySegment)
                {
                    _segmentPool.Return(segment);
                }

                throw;
            }
            finally
            {
                if (eventArgs != null)
                {
                    _eventArgsPool.Return(eventArgs);
                }
            }
        }

        public Task SendAsync(byte[] buffer)
        {
            return SendAsync(buffer, 0, buffer.Length);
        }

        public Task SendAsync(byte[] buffer, int count)
        {
            return SendAsync(buffer, 0, count);
        }

        public Task SendAsync(byte[] buffer, int offset, int count)
        {
            var context = new SendAsyncContext(this, buffer, offset, count);

            return _sendQueue.Enqueue(async (c) =>
            {
                await c.Client.InternalSendAsync(c.Buffer, c.Offset, c.Count);
            }, context);
        }

        private async Task InternalSendAsync(byte[] buffer, int offset, int count)
        {
            SocketAwaitableEventArgs eventArgs = null;

            try
            {
                eventArgs = _eventArgsPool.Take();

                eventArgs.UserToken = this;

                eventArgs.SetBuffer(buffer, offset, count);

                //if (_socket.Connected )

                await _socket.SendAwaitableAsync(eventArgs);
            }
            finally
            {
                if (eventArgs != null)
                {
                    _eventArgsPool.Return(eventArgs);
                }
            }
        }

        public async Task CloseAsync()
        {
            if (_socket == null || _socket.Connected == false) return;

            SocketAwaitableEventArgs eventArgs = null;

            try
            {
                eventArgs = _eventArgsPool.Take();

                eventArgs.UserToken = this;

                await _socket.DisconnectAwaitableAsync(eventArgs);
            }
            finally
            {
                if (eventArgs != null)
                {
                    _eventArgsPool.Return(eventArgs);
                }
            }
        }

        public void Dispose()
        {
            if (_socket == null) return;

            try
            {
                _socket.Shutdown(SocketShutdown.Both);
            }
            finally
            {
                _socket.Close();

                _socket = null;
            }
        }

        private class SendAsyncContext
        {
            public readonly byte[] Buffer;

            public readonly int Offset;

            public readonly int Count;

            public readonly TcpAsyncClient Client;

            public SendAsyncContext(TcpAsyncClient client, byte[] buffer, int offset, int count)
            {
                Client = client;
                Buffer = buffer;
                Offset = offset;
                Count = count;
            }
        }
    }
}
