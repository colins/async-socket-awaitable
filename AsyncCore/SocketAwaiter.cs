﻿using System;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncCore
{
    public class SocketAwaiter : INotifyCompletion
    {
        private static readonly Action Sentinel = () => { };

        private Action _continuation;
        private readonly SocketAsyncEventArgs _eventArgs;

        public SocketAwaiter(SocketAsyncEventArgs eventArgs)
        {
            _eventArgs = eventArgs;

            _eventArgs.Completed += (sender, args) =>
            {
                var prev = _continuation ?? Interlocked.CompareExchange(ref _continuation, Sentinel, null);

                prev?.Invoke();
            };
        }

        public SocketAwaiter GetAwaiter()
        {
            return this;
        }

        public bool IsCompleted { get; private set; }

        public SocketAsyncEventArgs EventArgs => _eventArgs;

        public void OnCompleted(Action continuation)
        {
            if (_continuation == Sentinel ||
                Interlocked.CompareExchange(ref _continuation, continuation, null) == Sentinel)
            {
                Task.Run(continuation);
            }
        }

        public void GetResult()
        {
            if (_eventArgs.SocketError != SocketError.Success)
                throw new SocketException((int)_eventArgs.SocketError);
        }

        internal void Reset()
        {
            _continuation = null;

            IsCompleted = false;
        }

        internal void Complete()
        {
            IsCompleted = true;
        }
    }
}
