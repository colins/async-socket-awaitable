﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AsyncCore;

namespace AsyncServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();

            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                cts.Cancel();

                eventArgs.Cancel = true;
            };

            var listenEndPoint = new IPEndPoint(IPAddress.Any, 44405);

            var segmentPool = new DefaultSegmentPool(1000, 8192);

            var eventArgsPool = new SocketAwaitableEventArgsPool();

            var protocol = new SampleProtocol();

            var listener = new TcpAsyncListener(listenEndPoint, segmentPool, eventArgsPool, protocol);

            listener.Start(1000);

            try
            {
                MainAsync(listener, protocol).Wait(cts.Token);
            }
            catch (OperationCanceledException)
            {
            }

            listener.Stop();

            Console.ReadLine();
        }

        static async Task MainAsync(TcpAsyncListener listener, IProtocol<TcpAsyncClient> protocol)
        {
            while (true)
            {
                var tcpClient = await listener.AcceptTcpAsyncClient();

                if (tcpClient == null) continue;

                await protocol.OnConnect(tcpClient);

                tcpClient.StartReceiving();
            }
        }
    }
}
