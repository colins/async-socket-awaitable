﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace AsyncCore
{
    public class DefaultSegmentPool : IPool<ArraySegment<byte>>
    {
        private readonly int _numOfSegments;
        private readonly int _bufferSize;
        private readonly byte[] _bufferBlock;
        private readonly ConcurrentBag<ArraySegment<byte>> _segmentsBag;

        public DefaultSegmentPool(int numOfSegments, int bufferSize)
        {
            _bufferSize = bufferSize;
            _numOfSegments = numOfSegments;
            _bufferBlock = new byte[numOfSegments * bufferSize];
            _segmentsBag = new ConcurrentBag<ArraySegment<byte>>(GetSegments());
        }

        public ArraySegment<byte> Take()
        {
            ArraySegment<byte> segment;

            if (_segmentsBag.TryTake(out segment))
            {
                return segment;
            }

            throw new InternalBufferOverflowException();
        }

        public void Return(ArraySegment<byte> segment)
        {
            _segmentsBag.Add(segment);
        }

        private IEnumerable<ArraySegment<byte>> GetSegments()
        {
            for (var i = 0; i < _numOfSegments; i++)
            {
                yield return new ArraySegment<byte>(_bufferBlock, i * _bufferSize, _bufferSize);
            }
        }
    }
}
