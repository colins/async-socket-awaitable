﻿using System;
using System.Threading.Tasks;

namespace AsyncCore
{
    public interface IProtocol<in TConnection>
    {
        Task OnConnect(TConnection connection);

        Task OnDisconnect(TConnection connection);

        Task OnReceive(TConnection connection, TcpAsyncReceiveResult result);

        Task OnError(TConnection connection, Exception exception);
    }
}
