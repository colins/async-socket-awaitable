﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncCore
{
    public class TcpAsyncListener
    {
        private Socket _listenSocket;
        private readonly IPEndPoint _localEndPoint;
        private readonly CancellationTokenSource _cts;

        private readonly IPool<ArraySegment<byte>> _segmentPool;
        private readonly IPool<SocketAwaitableEventArgs> _eventArgsPool;
        private readonly IProtocol<TcpAsyncClient> _protocol;

        public TcpAsyncListener(IPEndPoint listenEndPoint, IPool<ArraySegment<byte>> segmentPool,
            IPool<SocketAwaitableEventArgs> eventArgsPool, IProtocol<TcpAsyncClient> protocol)
        {
            _protocol = protocol;
            _localEndPoint = listenEndPoint;
            _segmentPool = segmentPool;
            _eventArgsPool = eventArgsPool;

            _cts = new CancellationTokenSource();
        }

        public Socket Socket => _listenSocket;

        public void Start(int backlog)
        {
            _listenSocket = new Socket(_localEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _listenSocket.Bind(_localEndPoint);

            try
            {
                _listenSocket.Listen(backlog);
            }
            catch (SocketException)
            {
                Stop();

                throw;
            }
        }

        public void Stop()
        {
            _cts.Cancel();

            if (_listenSocket != null)
            {
                _listenSocket.Close();

                _listenSocket = null;
            }

            _listenSocket = new Socket(_localEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public void BeginAccepting()
        {
            Task.Factory.StartNew(async () =>
            {
                while (!_cts.IsCancellationRequested)
                {
                    var tcpClient = await AcceptTcpAsyncClient();

                    if (tcpClient == null) continue;

                    await _protocol.OnConnect(tcpClient);
                }
            }, _cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);
        }

        public async Task<TcpAsyncClient> AcceptTcpAsyncClient()
        {
            SocketAwaitableEventArgs eventArgs = null;

            try
            {
                eventArgs = _eventArgsPool.Take();

                await _listenSocket.AcceptAwaitableAsync(eventArgs);

                var socket = eventArgs.AcceptSocket;

                if (socket == null) return null;

                return new TcpAsyncClient(socket, _segmentPool, _eventArgsPool, _protocol);
            }
            finally
            {
                if (eventArgs != null)
                {
                    _eventArgsPool.Return(eventArgs);
                }
            }
        }
    }
}
