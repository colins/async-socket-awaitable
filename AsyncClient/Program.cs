﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AsyncCore;

namespace AsyncClient
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            bool keepRunning = true;

            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                keepRunning = false;

                eventArgs.Cancel = true;
            };

            await Task.Delay(500);

            var segmentPool = new DefaultSegmentPool(1000, 8192);

            var eventArgsPool = new SocketAwaitableEventArgsPool();

            var protocol = new SampleProtocol();

            using (var client = new TcpAsyncClient(segmentPool, eventArgsPool, protocol))
            {
                var remoteEndPoint = new IPEndPoint(IPAddress.Loopback, 44405);

                try
                {

                    await client.ConnectAsync(remoteEndPoint).ConfigureAwait(false);

                    await client.SendAsync(new byte[] { 0xAA, 0xBB, 0xCC });

                    TcpAsyncReceiveResult result;

                    while (keepRunning && (result = await client.ReceiveAsync()) != null)
                    {
                        Console.WriteLine($"Received, Count: {result.BytesTransferred}");

                        for (int i = 0; i < result.BytesTransferred; i++)
                        {
                            Console.Write($"{result.Buffer[i]:X}");
                        }

                        Console.WriteLine();

                        //Parallel.For(0, 5, async i =>
                        //{
                            await client.SendAsync(result.Buffer, 0, result.BytesTransferred);
                        //});

                        segmentPool.Return(result.Segment);
                    }
                }
                catch (Exception e)
                {
                    Console.Write($"Error, {e}");
                }

                await client.CloseAsync();
            }

            Console.ReadLine();
        }
    }
}
