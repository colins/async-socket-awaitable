﻿using System;
using System.Threading.Tasks;

namespace AsyncCore
{
    public class SampleProtocol : IProtocol<TcpAsyncClient>
    {
        public SampleProtocol()
        {
            
        }

        public Task OnConnect(TcpAsyncClient connection)
        {
            Console.WriteLine($"OnConnect: {connection.Socket.RemoteEndPoint}");

            return Task.Delay(0);
        }

        public Task OnDisconnect(TcpAsyncClient connection)
        {
            Console.WriteLine($"OnDisconnect: {connection.Socket.RemoteEndPoint}");

            return Task.Delay(0);
        }

        public async Task OnReceive(TcpAsyncClient connection, TcpAsyncReceiveResult result)
        {
            Console.WriteLine($"OnReceive: {connection.Socket.RemoteEndPoint}, Count: {result.BytesTransferred}");

            await connection.SendAsync(result.Buffer, result.BytesTransferred);
        }

        public Task OnError(TcpAsyncClient connection, Exception exception)
        {
            Console.WriteLine($"OnError: {connection.Socket.RemoteEndPoint}, Exception: {exception}");

            return Task.Delay(0);
        }
    }
}
