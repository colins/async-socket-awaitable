﻿using System.Collections.Concurrent;
using System.Net.Sockets;

namespace AsyncCore
{
    public class SocketAwaitableEventArgsPool : IPool<SocketAwaitableEventArgs>
    {
        private readonly ConcurrentBag<SocketAwaitableEventArgs> _bag;
         
        public SocketAwaitableEventArgsPool()
        {
            _bag = new ConcurrentBag<SocketAwaitableEventArgs>();            
        }

        public SocketAwaitableEventArgs Take()
        {
            SocketAwaitableEventArgs eventArgs;

            if (_bag.TryTake(out eventArgs))
            {
                return eventArgs;
            }

            return new SocketAwaitableEventArgs();
        }

        public void Return(SocketAwaitableEventArgs value)
        {
            value.AcceptSocket = null;
            value.SetBuffer(null, 0, 0);
            value.SocketFlags = SocketFlags.None;
            value.UserToken = null;
            value.RemoteEndPoint = null;

            _bag.Add(value);
        }
    }
}
